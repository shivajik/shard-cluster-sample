Certainly! Here's a revised README file:

---

# Setting Up a Sharded MongoDB Cluster

This guide will help you set up a sharded MongoDB cluster on your local machine. The cluster consists of config servers, shard servers (sh1 and sh2), and a mongos router.

## Prerequisites

Before starting, make sure you have the following installed:

- MongoDB
- Access to your local machine's terminal or command prompt

## Step 1: Config Server Setup

1. Open a terminal or command prompt.

2. Start the config servers by running the following commands:

   ```
   mongod -f D:\socium\shard\pointse-sharded-cluster\cf1\mongod.cfg
   mongod -f D:\socium\shard\pointse-sharded-cluster\cf2\mongod.cfg
   mongod -f D:\socium\shard\pointse-sharded-cluster\cf3\mongod.cfg
   ```

3. Connect to one of the config servers using mongosh:

   ```
   mongosh --port 31000
   ```

4. Initialize the config replica set (config_server):

   ```
   rs.initiate({
       _id: "config_server",
       configsvr: true,
       members: [
           { _id: 0, host: "localhost:31000" },
           { _id: 1, host: "localhost:32000" },
           { _id: 2, host: "localhost:33000" }
       ]
   })
   ```

5. Verify the config server replica set configuration:

   ```
   rs.conf();
   ```

## Step 2: Shard Server Setup sh1

1. Open a new terminal or command prompt.

2. Start the shard servers for sh1:

   ```
   mongod -f D:\socium\shard\pointse-sharded-cluster\sh1\a\mongod.cfg
   mongod -f D:\socium\shard\pointse-sharded-cluster\sh1\b\mongod.cfg
   mongod -f D:\socium\shard\pointse-sharded-cluster\sh1\c\mongod.cfg
   ```

3. Connect to one of the shard servers using mongosh:

   ```
   mongosh --port 40000
   ```

4. Initialize the sh1 shard replica set:

   ```
   rs.initiate({
       _id: "sh1",
       members: [
           { _id: 0, host: "localhost:40000" },
           { _id: 1, host: "localhost:40001" },
           { _id: 2, host: "localhost:40002" }
       ]
   })
   ```

5. Verify the sh1 shard replica set configuration:

   ```
   rs.conf();
   ```

Certainly! Here are the steps to set up the shard servers for sh2:

## Step 3: Shard Server Setup for sh2

1. Open a new terminal or command prompt.

2. Start the shard servers for sh2:

   ```
   mongod -f D:\socium\shard\pointse-sharded-cluster\sh2\a\mongod.cfg
   mongod -f D:\socium\shard\pointse-sharded-cluster\sh2\b\mongod.cfg
   mongod -f D:\socium\shard\pointse-sharded-cluster\sh2\c\mongod.cfg
   ```

3. Connect to one of the shard servers using mongosh:

   ```
   mongosh --port 50000
   ```

4. Initialize the sh2 shard replica set:

   ```
   rs.initiate({
       _id: "sh2",
       members: [
           { _id: 0, host: "localhost:50000" },
           { _id: 1, host: "localhost:50001" },
           { _id: 2, host: "localhost:50002" }
       ]
   })
   ```

5. Verify the sh2 shard replica set configuration:

   ```
   rs.conf();
   ```

## Step 4: Mongos Router Setup

1. Open another terminal or command prompt.

2. Start the mongos router:

   ```
   mongos -f D:\socium\shard\pointse-sharded-cluster\mongos\mongod.cfg
   ```

3. Connect to the mongos router using mongosh:

   ```
   mongosh --port 29999
   ```

4. Perform shard operations, such as adding shards:

   ```
   sh.help()
   sh.status()
   sh.addShard("sh1/localhost:40000")
   sh.addShard("sh2/localhost:50000")
   ```

## Conclusion

You have successfully set up a sharded MongoDB cluster on your local machine. You can now start using the cluster for your applications.

---
